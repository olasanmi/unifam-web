@extends('layouts.app')

@section('content')
<div class="container d-flex justify-content-center p-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card bg_verification_card">
                <center>
                    <div class="img_card_verify">
                        <img src="{{url('assets/images/logo2.png')}}" width="50px" height="50px" />
                    </div>
                </center>
                <div class="card-body row">
                <div class="col-12">
                    <center>
                        <img src="{{url('assets/images/icon-1.png')}}" width="50px" height="50px" />
                    </center>
                </div>
                <div class="col-12 mt-4 text-center">
                    <h4 class="title_card_verify">Thank you!</h4>
                </div>
                <div class="col-12 mt-1 text-center">
                    <span class="verify_text">Your application has been received.</span>
                </div>
                <div class="col-12 text-center">
                    <span class="verify_text">You will receive an email with instructions to confirm your account and log in.</span>
                </div>
                <div class="col-12">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                    @endif
                </div>
                <div class="col-12 text-center mt-4">
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn terciary verify_done">Resend <i class="fa fa-chevron-right ml-2"></i></button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
