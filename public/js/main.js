function Edad(FechaNacimiento) {
	var fechaNace = new Date(FechaNacimiento);
	var fechaActual = new Date()
	var mes = fechaActual.getMonth();
	var dia = fechaActual.getDate();
	var año = fechaActual.getFullYear();
	fechaActual.setDate(dia);
	fechaActual.setMonth(mes);
	fechaActual.setFullYear(año);
	edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
	return edad
}

function calEdad (d) {
	var fecha = document.getElementById('birthday').value;
	var edad = Edad(fecha);
	document.getElementById('patient-edad').value = edad
}


	function setMinInDate () {
		var date = document.getElementById('date_cita')
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
	
		today = yyyy + '-' + mm + '-' + dd;
		if(date){
			console.log(today);
			date.setAttribute('min', today)
		}
	}
	setMinInDate();

$(document).ready(function(){
	$(".tabs li a").click(function() {
		
		// Active state for tabs
		$(".tabs li a").removeClass("active");
		$(this).addClass("active");
		
		// Active state for Tabs Content
		$(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
		$(this.rel).fadeIn(500).addClass("tab_content_active");
		
	});	

});

$(document).ready(function(){
	$("#enable-address").click(function(){
		$("#address-textarea").enabled();
		return false
	})
})
