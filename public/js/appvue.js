window.onload=function(){
	new Vue({
		el: '#app',
		components: {  
		},data:{
			doctors: [],
			speciality: [],
			provinces: [],
			doctors: [],
			numbersResult: 6,
			page: 1,
			totalPages: null,
		},methods:{
			filterDoctors: function(){
				this.doctors = []
				let province = document.getElementById("province").value
				let speciality = document.getElementById("speciality").value
				if (province != '' && speciality != '') {
					axios({ url: '/dashboard/doctor/'+province+'/'+speciality, data: this.data, method: 'get' })
					.then(response => {
						this.doctors = response.data
					})
					.catch(err => {
						console.log(err)
					})
				} else {
					alert('Selecciona la provincia donde buscas el doctor')
				}
				return false;
			},
			filterData: function () {
				axios({ url: '/filter/data', method: 'get' })
				.then(response => {
					this.provinces = response.data.data.provinces
					this.speciality = response.data.specialities[0]
				})
				.catch(err => {
					console.log(err)
				})
			},
			getHightLightDoctor () {
				axios({ url: '/filter/doctor/hightlight', method: 'get' })
				.then(response => {
					this.doctors = response.data
				})
				.catch(err => {
					console.log(err)
				})
			},
			filterDoctor () {
				this.doctors = []
				let name = document.getElementById("doctor-name").value
				let especialidad = document.getElementById("especialidad").value
				let lugar = document.getElementById("lugar").value
				axios({ url: '/filter/doctor/'+name+'/'+ especialidad +'/'+ lugar, method: 'get' })
				.then(response => {
					this.doctors = response.data
					this.numbersResult = 6
					this.page = 1
				})
				.catch(err => {
					console.log(err)
				})
			},
			next () {
				this.totalPages = this.doctors.length / 5
				this.page += 1
			},
			back () {
				if (this.page > 1) {
					this.page -= 1
				}
			}
		},beforeMount(){
			if (document.getElementById('landing')) {
				this.filterData()
				this.getHightLightDoctor()
			}
		}
	})
}