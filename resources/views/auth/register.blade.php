@extends('layouts.app')
@section('content')
<div class="d-flex justify-content-center p-2">
    <div class="card card-login mt-4">
        <center>
            <div class="img_card_login">
                <img src="{{url('assets/images/logo.png')}}" />
            </div>
        </center>
        <div class="card-header text-center">
            <h4 class="text_header_title">Welcome to UNIFAM</h4>
            <p>Please fill out the following form to apply</p>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label>Your name</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Please enter your name">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-1">
                        <label>Your address</label>
                        <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" placeholder="Please enter your address">

                        @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-1">
                        <label>Your email</label>
                         <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Please enter your email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-1">
                        <label>Your password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Please type a password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror   
                    </div>
                    <div class="col-md-12 col-sm-12 mt-3">
                        <button style="margin-left: 0px" type="submit" class="ml234 btn-block btn primary 8u7x7wwidth-80">
                            {{ __('Sign In') }}
                        </button>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
