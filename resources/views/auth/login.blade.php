@extends('layouts.app')
@section('content')
<div class="d-flex justify-content-center p-2">
    <div class="card card-login mt-4">
        <center>
            <div class="img_card_login">
                <img src="{{url('assets/images/logo.png')}}" />
            </div>
        </center>
        <div class="card-header text-center">
            <h4 class="text_header_title">Welcome to UNIFAM</h4>
            <p>Please sign in to enter to your account</p>
            <p class="mt_-10">Or sign up for an account</p>
        </div>
        <div class="card-body">
            <form class="mt-2" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label>Your email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror w-502 hyper-input" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-3">
                        <label>Password</label>
                        <input placeholder="" id="password" type="password" class="form-control @error('password') is-invalid @enderror w-502 hyper-input" name="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-12 col-sm-12 mt-3">
                        <button style="margin-left: 0px" type="submit" class="ml234 btn-block btn primary 8u7x7wwidth-80">
                            {{ __('Sign In') }}
                        </button>
                    </div>
                    <div class="col-md-12 text-left text-white mt-3">
                        <a href="{{route('password.request')}}" class="">Forgot your password?</a>
                    </div>
                    <div class="col-md-12 bb-1">
                    </div>
                    <div class="col-md-3 col-xs-3 col-3 mt-3">
                        <hr />
                    </div>
                    <div class="col-md-6 col-sm-6 col-6 mt-3">
                        <a href="#" class="btn btn-hyper btn-block">Don`t have an account?</a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-3 mt-3">
                        <hr />
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12 col-sm-12 mt-3">
                    <a href="{{url('/register')}}">
                        <button style="margin-left: 0px" class="ml234 btn-block btn btn-primary 8u7x7wwidth-80">
                            {{ __('Sign Up') }}
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
