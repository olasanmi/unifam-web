<?php

namespace App\Http\Controllers;

use App\Loan;
use Illuminate\Http\Request;
use App\User;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // loans get all
        $loans = Loan::all();
        return view('dashboard.loans.index')->with('loans', $loans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::all();
        return view('dashboard.loans.add')->with('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $loans = Loan::create([
            'loans_amount' => $request->amount,
            'annual_interest_rate' => $request->interest,
            'terms_in_year' => $request->termInYear,
            'first_payment' => $request->first_payment,
            'payment_frequency' => $request->paymentFrequency,
            'type' => $request->paymentFrequency,
            'compound_period' => $request->compoundPeriod,
            'payment_type' => $request->paymentType,
            'rate_per_period' => $request->rate,
            'numbers_payment' => $request->numberOfPayment,
            'total_payment' => $request->totalToPay,
            'total_interest' => $request->totalInterestToPay
        ]);

        return response()->json(['success' => 'Success.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
