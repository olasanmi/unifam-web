<?php

namespace App\Http\Requests\Trainer;

use Illuminate\Foundation\Http\FormRequest;

class TrainerUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'phone' => ['required', 'max:20'],
            'address' => ['required', 'max:90'],
            'name' => ['required', 'max:90'],
            'first_name' => ['required', 'max:90'],
            'last_name' => ['max:90'],
            'password' => ['confirmed', 'max:20'],
            'age' => ['required', 'max:3'],
            'gender' => ['required'],
            'email' => ['required', 'email'],
            'speciality' => ['required'],
            'foto_trainer' => ['mimes:jpeg,png']
        ];
    }
}
