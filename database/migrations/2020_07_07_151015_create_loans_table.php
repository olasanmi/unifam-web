<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->nullable();
            $table->float('loans_amount',50,30)->nullable();
            $table->string('annual_interest_rate',50,2)->nullable();
            $table->integer('terms_in_year')->nullable();
            $table->date('first_payment')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->string('type')->nullable();
            $table->string('compound_period')->nullable();
            $table->string('payment_type')->nullable();
            $table->float('rate_per_period',50,2)->nullable();
            $table->integer('numbers_payment')->nullable();
            $table->float('total_payment',50,2)->nullable();
            $table->float('total_interest',50,2)->nullable();
            $table->integer('payment_period')->nullable();
            $table->integer('montly_payment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
