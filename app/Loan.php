<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'reference',
        'loans_amount',
        'annual_interest_rate',
        'terms_in_year',
        'first_payment',
        'payment_frequency',
        'type',
        'compound_period',
        'payment_type',
        'rate_per_period',
        'numbers_payment',
        'total_payment',
        'total_interest',
        'payment_period',
        'montly_payment'
    ];

    // relations
    // user relations
    public function User () {
        return $this->belongsTo('App\User');
    }
}
