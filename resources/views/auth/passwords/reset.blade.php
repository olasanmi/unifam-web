@extends('layouts.app')

@section('content')
<div class="container d-flex justify-content-center">
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="max-width: 400px; margin-top: 3rem;">
                <center>
                    <div class="img_card_verify">
                        <img src="{{url('assets/images/logo.png')}}" width="50px" height="50px" />
                    </div>
                </center>
                <div class="card-body row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
    
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Your email</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>Your Password</label>
                                    <input id="password" placeholder="Please enter your password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>Password Confirm</label>
                                    <input placeholder="Please confirm password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 col-sm-12 mt-3">
                                    <button style="margin-left: 0px" type="submit" class="ml234 btn-block btn primary 8u7x7wwidth-80">
                                        {{ __('Reset Password') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
