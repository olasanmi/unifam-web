<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relations
    public function Loans () {
        return $this->hasMany('App\Loans', 'user_id');
    }
    //machines
    public function Machine () {
        return $this->hasOne('App\Machine');
    }
    //trainer lesson
    public function Lesson () {
        return $this->hasMany('App\Lesson');
    }
    //user cupes in any lesson
    public function LessonCupes () {
        return $this->hasMany('App\LessonCupe');
    }
    //sucription
    public function Suscription() {
        return $this->hasOne('App\Suscription', 'user_id');
    }

    //facturas
    public function Facturings () {
        return $this->hasMany('App\Facturing');
    }
}
