<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
//models
use App\User;
use App\Profile;
use App\Plan;
use App\Suscription;
use App\Facturing;
//helpers
use App\Helpers\Helper;
//request
use App\Http\Requests\Client\ClientStore;
use App\Http\Requests\Client\ClientUpdate;
use App\Http\Requests\Client\assingSuscription;
use App\Http\Requests\Trainer\TrainerStore;
use App\Http\Requests\Trainer\TrainerUpdate;
//qr
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UsersController extends Controller
{
    //#########client methdos
    //index
    public function main() {
    	return view('dashboard.main');
    }

}
